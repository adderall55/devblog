/*
    This file is part of the ChipWhisperer Example Targets
    Copyright (C) 2012-2015 NewAE Technology Inc.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hal.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define IDLE 0
#define KEY 1
#define PLAIN 2

#define BUFLEN 64

uint8_t memory[BUFLEN];
uint8_t tmp[BUFLEN];
char asciibuf[BUFLEN];
uint8_t pt[16];

void my_puts(char *c) {
  while(*c!=0)
    putch(*c++);
}

void my_read(char *b, int len) {
  while(len--) {
    *b++ = getch();
  }
}

void foo(volatile char *x, unsigned n){
	while(n--) {
		*x++;
	}
}

//char buf[N*2+3] = {0};

char *q = "aE!!23bY";
#define N 8


int main(void) {
  platform_init();
  init_uart();
  trigger_setup();
  char p[N];
  //char *q = &buf[2+N];
  //memory layout buf = {0,p,0,q,0}
  while(1){
    //my_read(q, N);
    my_read(p, N);
    trigger_high();
	unsigned c = 1;
	for(int i =0;i<N;i++){
		if(p[i]!=q[i]) {
			c = 0;
			break;
		}
	}
    trigger_low();
    if(c) {
      my_puts("Pass");
    }
    else {
      my_puts("Fail");
    }
  }
  return 1;
}


