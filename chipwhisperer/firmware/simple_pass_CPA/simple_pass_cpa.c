/*
    This file is part of the ChipWhisperer Example Targets
    Copyright (C) 2012-2015 NewAE Technology Inc.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hal.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

void my_puts(char *c) {
  while(*c!=0)
    putch(*c++);
}
void my_read(char *b, int len) {
  while(len--) {
    *b++ = getch();
  }
}
int main(void) {
  platform_init();
  init_uart();
  trigger_setup();
  char q[8];
  char p[sizeof(q)];
  while(1) {
    for(unsigned i = 0;i<sizeof(q);i++) {
      p[i] = 0;
      q[i] = 1;
    }
    my_read(q, sizeof(q));
    my_read(p, sizeof(p));
    trigger_high();
    unsigned c = 0;
    for(unsigned i=0; i<sizeof(q);i++) {
      c  |= q[i] ^ p[i];
    }
    trigger_low();
    if(c == 0) {
      my_puts("Pass");
    }
    else{
      my_puts("Fail");
    }
  }
  return 0;
}
