/*
    This file is part of the ChipWhisperer Example Targets
    Copyright (C) 2012-2017 NewAE Technology Inc.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "aes-independant.h"
#include "hal.h"
#include "simpleserial.h"
#include <stdint.h>
#include <stdlib.h>

uint8_t get_mask(uint8_t* m)
{
  //aes_indep_mask(m);
  return 0x00;
}

uint8_t get_key(uint8_t* k)
{
	//aes_indep_key(k);
	return 0x00;
}

void trig_hi() {
  trigger_high();
}
void trig_lo() {
  trigger_low();
}

uint8_t get_pt(uint8_t* pt)
{
	//trigger_high();
	//aes_indep_enc(pt); /* encrypting the data block */
	//trigger_low();
	simpleserial_put('r', 16, pt);
	return 0x00;
}

uint8_t reset(uint8_t* x)
{
    // Reset key here if needed
	return 0x00;
}

#define T int64_t
T mod(T x,T n){
  return x%n;
}
T Xpow(T x, T p, T n) {
  if (p == 0) return 1;
  if (p == 1) return x;
  T tmp = Xpow(x, p/2, n);
  tmp = mod(tmp*tmp,n);
  if (p%2 == 0)
    return tmp;
  else
    return mod(tmp * x, n);
}
T gcd(T p, T q) {
  if(q==0) {
    return p;
  }
  return gcd(q, p % q);
}
T lcm(T p,T q) {
  return p*q/gcd(p,q);
}
void egcd(T e, T phi, T *g, T *y, T *x) {
  if(e==0) {
    *g = phi;
    *y = 0;
    *x = 1;
    return;
  }
  egcd(phi%e,e,g,y,x);
  T a = *y;
  *y = *x - (phi / e) * *y;
  *x = a;
}

T modinv(T e, T phi) {
  T g, x, y;
  egcd(e, phi, &g, &x, &y);
  return mod(x,phi);
}  
T crt(T m1, T m2, T p, T q, T qi) {
  T h  = (qi * (m1 - m2)) % p;
  return m2 + h * q;
}
T dec(T c, T p, T q, T dp, T dq, T qi) {
  trigger_high();
  T m1 = Xpow(c, dp, p);
  
  T m2 = Xpow(c, dq, q);
  trigger_low();
  return crt(m1, m2, p, q, qi);
}

uint8_t run(uint8_t * pt) {
  T p  = 23;
  T q  = 17;
  T e  = 3;
  T c  = 230;
  T d  = modinv(e, lcm(p - 1, q - 1));
  T dp = mod(d, p - 1);
  T dq = mod(d, q - 1);
  T qi = modinv(q, p);
  //trigger_high();
  T m  = dec(c, p, q, dp, dq, qi);
  uint8_t x[16] = {0};
  memcpy(x,&m,sizeof(m));
  //trigger_low();
  simpleserial_put('r', 16, x);
  return 0;
}

int main(void)
{
	uint8_t tmp[KEY_LENGTH] = {DEFAULT_KEY};

    platform_init();
    init_uart();
    trigger_setup();

	//aes_indep_init();
	//aes_indep_key(tmp);

    /* Uncomment this to get a HELLO message for debug */

    putch('h');
    putch('e');
    putch('l');
    putch('l');
    putch('o');
    putch('\n');
    

    simpleserial_init();
    simpleserial_addcmd('k', 16, get_key);
    simpleserial_addcmd('p', 16,  run);
    simpleserial_addcmd('x',  0,   reset);
    simpleserial_addcmd('m', 18, get_mask);
    while(1)
        simpleserial_get();
}
