#if 1
#include "hal.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define __IO volatile
typedef struct
{
  __IO uint32_t MODER;        /*!< GPIO port mode register,                     Address offset: 0x00      */
  __IO uint32_t OTYPER;       /*!< GPIO port output type register,              Address offset: 0x04      */
  __IO uint32_t OSPEEDR;      /*!< GPIO port output speed register,             Address offset: 0x08      */
  __IO uint32_t PUPDR;        /*!< GPIO port pull-up/pull-down register,        Address offset: 0x0C      */
  __IO uint32_t IDR;          /*!< GPIO port input data register,               Address offset: 0x10      */
  __IO uint32_t ODR;          /*!< GPIO port output data register,              Address offset: 0x14      */
  __IO uint32_t BSRR;         /*!< GPIO port bit set/reset register,      Address offset: 0x1A */
  __IO uint32_t LCKR;         /*!< GPIO port configuration lock register,       Address offset: 0x1C      */
  __IO uint32_t AFR[2];       /*!< GPIO alternate function low register,  Address offset: 0x20-0x24 */
  __IO uint32_t BRR;          /*!< GPIO bit reset register,                     Address offset: 0x28      */
} GPIO_TypeDef;
#define PERIPH_BASE           ((uint32_t)0x40000000U)              /*!< Peripheral base address in the alias region */
#define AHB2PERIPH_BASE       (PERIPH_BASE + 0x08000000)
#define GPIOA_BASE            (AHB2PERIPH_BASE + 0x00000000)
#define GPIOA                 ((GPIO_TypeDef *) GPIOA_BASE)
#define GPIO_PIN_12           ((uint32_t)0x1000U)

void my_puts(char *c, unsigned n) {
  while(n--)
    putch(*c++);
}
volatile int x = 0;
int main(void) {
  platform_init();
  init_uart();
  trigger_setup();
  my_puts("helo",4);
  volatile uint32_t *BSRR = &(GPIOA->BSRR);
  volatile uint32_t *BRR  = &(GPIOA->BRR);
  if(x==1) goto done;
loop:
  *BSRR = GPIO_PIN_12;
  *BRR  = GPIO_PIN_12;
  goto loop;
done:
  my_puts("pass",4);
  return 0;
}
#else
#include "hal.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
void my_puts(char *c) {
  while(*c!=0)
    putch(*c++);
}
void my_read(char *b, int len) {
  while(len--) {
    *b++ = getch();
  }
}
int main(void) {
  platform_init();
  init_uart();
  trigger_setup();
  char q[8];
  char p[sizeof(q)];
  while(1) {
    for(unsigned i = 0;i<sizeof(q);i++) {
      p[i] = 0;
      q[i] = 1;
    }
    my_read(q, sizeof(q));
    my_read(p, sizeof(p));
    trigger_high();
    unsigned c = 0;
    for(unsigned i=0; i<sizeof(q);i++) {
      c  |= q[i] ^ p[i];
    }
    trigger_low();
    if(c == 0) {
      my_puts("Pass");
    }
    else{
      my_puts("Fail");
    }
  }
  return 0;
}
#endif
